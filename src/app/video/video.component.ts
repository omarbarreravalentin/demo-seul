import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

/**
 * Constante para hacer refrencia a la libreria veridas makeVDVideoWidget
 */
declare const makeVDVideoWidget: any;
@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css']
})
export class VideoComponent implements OnInit{

  constructor(
    private router: Router) {

    }

  ngOnInit() {
    const VDVideo = makeVDVideoWidget();
    VDVideo({
      targetSelector: '#target',
      documents: []
    });
    const detectionEvents = [
      'VDVIDEO_mounted',
      'VDVideo_unmounted',
      'VDVideo_mountFailure',
      'VDVideo_cameraStarted',
      'VDVideo_cameraFailure',
      'VDVideo_standardVideoRecorderStart',
      'VDVideo_alternativeVideoRecorderStart',
      'VDVideo_standardVideoOutput',
      'VDVideo_alternativeVideoOutput',
      'VDVideo_detectionTimeout'
    ];
  }
}

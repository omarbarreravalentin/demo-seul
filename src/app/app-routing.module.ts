import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DocumentoComponent } from './documento/documento.component';
import { SelfieComponent } from './selfie/selfie.component';
import { VideoComponent } from './video/video.component';

const routes: Routes = [
  { path: '', component: DocumentoComponent },
  { path: 'selfie', component: SelfieComponent },
  { path: 'video', component: VideoComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit, OnDestroy } from '@angular/core';
import { timeout } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

/**
 * Constante para hacer refrencia a la libreria veridas makeVDSelfieWidget
 */
declare const makeVDDocumentWidget: any;
/**
 * Constante para hacer refrencia a la funcion loadWASM, para conocer si el .wasm ha sido cargado
 */
declare function loadWASM(): any;
/**
 * Constante para conocer si se ha ingresado al metodo de envio de anverso
 */
let cicloanv: boolean;

@Component({
  selector: 'app-documento',
  templateUrl: './documento.component.html',
  styleUrls: ['./documento.component.css']
})
export class DocumentoComponent implements OnInit, OnDestroy {
  
  /**
  Intrvalo
  */
  public intervalCapture: any;

  constructor(
    private router: Router) {
    cicloanv = false;
  }

  /**
   * Metodo para inicializar llamada a libreria makeVDDocumentWidget, asi mismo genera todos los listener donde se escucharan
   * los estados de desarrolo de la captura del documento
   */
  ngOnInit() {
    setTimeout(() => {
      const VDDocument = makeVDDocumentWidget();
      VDDocument({
        targetSelector: '#target',
        documents: ['MX'],
        docCaptureVerticalPortraitMode:true,
        docLibHelperPath: '/docLibHelper',
        documentButtonCapture: true,
        documentButtonCaptureText: 'Tomar foto'
      });
      const detectionEvents = [
        'VDDocument_mounted',
        'VDDocument_unmounted',
        'VDDocument_mountFailure',
        'VDDocument_cameraStarted',
        'VDDocument_cameraFailure',
        'VDDocument_obverseDetection',
        'VDDocument_reverseDetection',
        'VDDocument_detectionTimeout'
      ];
      detectionEvents.forEach((event) => {
        addEventListener(event, this.documentDetectionEvent.bind(this), true);
      });
    }, 2000);
  }

  documentDetectionEvent(e:any) {
    if (e && e.type) {
      const operation = e.type;
      switch (operation) {
        case 'VDDocument_reverseDetection':
          this.router.navigate(['/selfie']);
          break;
        case 'VDDocument_obverseDetection':
          if (!cicloanv) {
            cicloanv = true;
          }
          break;
      }
    }
  }

  /**
  * Metodo para ejecutar unsubscribe del componente, y limpiar intervals
  */
  ngOnDestroy() {
    try {
      clearInterval(this.intervalCapture);
    } catch (err) { }
  }

}

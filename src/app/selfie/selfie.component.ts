import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
/**
 * Constante para hacer refrencia a la libreria veridas makeVDSelfieWidget
 */
declare const makeVDSelfieWidget: any; // valores permitidos makeVDAliveWidget(), makeVDSelfieWidget()
let ciclof: boolean;
@Component({
  selector: 'app-selfie',
  templateUrl: './selfie.component.html',
  styleUrls: ['./selfie.component.css']
})
export class SelfieComponent implements OnInit {
  public sdk: string = "VDSelfie";

  constructor(
    private router: Router) {
    let ciclof: boolean = false;

  }

  ngOnInit() {
    this.sdk = 'VDSelfie'; // Valores permitidos VDSelfie, VDAlive
    const VDAlive = makeVDSelfieWidget(); // valores permitidos makeVDAliveWidget(), makeVDSelfieWidget()
    VDAlive({
      targetSelector: '#target'
    });
    const detectionEvents = [
      this.sdk + '_mounted',
      this.sdk + '_unmounted',
      this.sdk + '_mountFailure',
      this.sdk + '_cameraStarted',
      this.sdk + '_cameraFailure',
      this.sdk + '_faceDetection',
      this.sdk + '_continueClicked',
      this.sdk + '_detectionTimeout'
    ];

    detectionEvents.forEach((event) => {
      addEventListener(event, this.fotoDetectionEvent.bind(this), true);
    });
  }

  fotoDetectionEvent(e: any) {
    if (e && e.type) {
      const operation = e.type;
      console.log(operation)
      switch (operation) {
        case this.sdk + '_faceDetection':
          if (!ciclof) {
            ciclof = true;
            this.router.navigate(['/video']);
          }
          break;
        case this.sdk + '_continueClicked':
          this.router.navigate(['/video']);
          break;

        default:
          break;
      }
    }
  }

}

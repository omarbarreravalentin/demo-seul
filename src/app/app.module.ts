import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SelfieComponent } from './selfie/selfie.component';
import { VideoComponent } from './video/video.component';
import { DocumentoComponent } from './documento/documento.component';

@NgModule({
  declarations: [			
    AppComponent,
      SelfieComponent,
      VideoComponent,
      DocumentoComponent
   ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
